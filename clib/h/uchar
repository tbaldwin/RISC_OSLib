/* Copyright 2020 RISC OS Open Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#pragma force_top_level
#pragma include_only_once

/* uchar.h: ISO 'C' (9899:2018) library header, section 7.28 */

/*
 * The header <uchar.h> declares types and functions for manipulating
 * Unicode characters.
 */

#ifndef __uchar_h
#define __uchar_h

#ifndef __size_t
#  define __size_t 1
   typedef unsigned int size_t;    /* from <stddef.h> */
#endif

#ifndef __mbstate_t
#  define __mbstate_t 1
   typedef struct __mbstate_struct /* from <wchar.h> */
   {
      wchar_t __c; /* current character? */
      int __n; /* pending bytes? */
      wchar_t __min;
   } mbstate_t;
   /*
    * an object type other than an array type that can hold the conversion
    * state information necessary to convert between sequences of multibyte
    * characters and wide characters
    */
#endif

typedef unsigned short char16_t;
   /*
    * an unsigned integer type used for 16-bit characters
    */

typedef unsigned int char32_t;
   /*
    * an unsigned integer type used for 32-bit characters
    */

size_t mbrtoc16(char16_t * restrict /*pc16*/, const char * restrict /*s*/,
                size_t /*n*/, mbstate_t * restrict /*ps*/);
   /*
    * inspects at most n bytes beginning with the byte pointed to by s to
    * determine the number of bytes needed to complete the next multibyte
    * character (including any shift sequences). If the function determines that
    * the next multibyte character is complete and valid, it determines the
    * value of the corresponding wide character and then, if pc16 is not a null
    * pointer, stores that value in the object pointed to by pc16. If the
    * corresponding wide character is the null wide character, the resulting
    * state described is the initial conversion state.
    */
size_t c16rtomb(char * restrict /*s*/, char16_t /*c16*/,
                mbstate_t * restrict /*ps*/);
   /*
    * determines the number of bytes need to represent the multibyte character
    * that corresponds to the wide character given by c16 (including any
    * shift sequences), and stores the multibyte character representation in the
    * array whose first element is pointed to by s. At most MB_CUR_MAX bytes
    * are stored. If c16 is a null wide character, a null byte is stored,
    * preceded by any shift sequence needed to restore the initial shift state;
    * the resulting state described is the initial conversion state.
    */
size_t mbrtoc32(char32_t * restrict /*pc32*/, const char * restrict /*s*/,
                size_t /*n*/, mbstate_t * restrict /*ps*/);
   /*
    * inspects at most n bytes beginning with the byte pointed to by s to
    * determine the number of bytes needed to complete the next multibyte
    * character (including any shift sequences). If the function determines that
    * the next multibyte character is complete and valid, it determines the
    * value of the corresponding wide character and then, if pc32 is not a null
    * pointer, stores that value in the object pointed to by pc32. If the
    * corresponding wide character is the null wide character, the resulting
    * state described is the initial conversion state.
    */
size_t c32rtomb(char * restrict /*s*/, char32_t /*c32*/,
                mbstate_t * restrict /*ps*/);
   /*
    * determines the number of bytes need to represent the multibyte character
    * that corresponds to the wide character given by c32 (including any
    * shift sequences), and stores the multibyte character representation in the
    * array whose first element is pointed to by s. At most MB_CUR_MAX bytes
    * are stored. If c32 is a null wide character, a null byte is stored,
    * preceded by any shift sequence needed to restore the initial shift state;
    * the resulting state described is the initial conversion state.
    */

#endif

/* end of uchar.h */
