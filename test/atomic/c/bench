/* Copyright 2022 RISC OS Open Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifdef USE_ROATOMIC
#include "roatomic.h"
#else
#include <stdatomic.h>
#endif
#include "noatomic.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>

#define CONCAT(X,Y) CONCAT2(Y,X)
#define CONCAT2(Y,X) X##Y
#define BFUNC(name) CONCAT(CONCAT(PREFIX,name),CONCAT(ORDER,SIZE))

typedef clock_t (*bench_func)(uint32_t iters);

bool output_csv = false;

static int64_t bench_internal(bench_func f)
{
  uint32_t iters = 1024;
  clock_t t = 0;
  /* Get about a seconds worth of precision */
  while (t < CLOCKS_PER_SEC)
  {
    iters *= 2;
    t = f(iters);
  }
  /* Return iterations per second */
  return (((int64_t) iters)*CLOCKS_PER_SEC)/t;
}

static void benchmark(const char *name,bench_func atomic,bench_func reference)
{
  if (!output_csv)
  {
    printf("%s: ",name);
  }
  int64_t iter_a = bench_internal(atomic);
  int64_t iter_r = bench_internal(reference);
  if (!output_csv)
  {
    /* Just display percentage performance relative to the base */
    printf("%lld%% (%lld -> %lld)\n",(iter_a*100)/iter_r,iter_r,iter_a);
  }
  else
  {
    printf("%s,%lld,%lld\n",name,iter_r,iter_a);
  }
}

#define BENCHMARK(N) benchmark(#N,atomic_bench_##N,noatomic_bench_##N)
#define BENCHMARKS(N) do { BENCHMARK(N##_1); BENCHMARK(N##_2); BENCHMARK(N##_4); BENCHMARK(N##_8); } while(0)

void consume(void *val)
{
  /* Just do nothing */
}

#define ATOMIC uchar
#define BASE uint8_t
#define SIZE _1
#include "benchi.c"
#define ATOMIC ushort
#define BASE uint16_t
#define SIZE _2
#include "benchi.c"
#define ATOMIC uint
#define BASE uint32_t
#define SIZE _4
#include "benchi.c"
#define ATOMIC ullong
#define BASE uint64_t
#define SIZE _8
#include "benchi.c"

#define FLAG_BENCHMARK(PREFIX,ORDER,ORDER2) \
static clock_t PREFIX##_bench_flag_##ORDER(uint32_t iters) \
{ \
  PREFIX##_flag *f = (PREFIX##_flag *) malloc(sizeof(PREFIX##_flag)); \
  PREFIX##_flag_clear_explicit(f,memory_order_seq_cst); \
  clock_t t = clock(); \
  bool set = false; \
  while(iters--) \
  { \
    set |= PREFIX##_flag_test_and_set_explicit(f,memory_order_##ORDER); \
    PREFIX##_flag_clear_explicit(f,memory_order_##ORDER2); \
  } \
  t = clock()-t; \
  assert(!set); \
  free((void *) f); \
  return t; \
}

FLAG_BENCHMARK(atomic,relaxed,relaxed)
FLAG_BENCHMARK(atomic,acq_rel,release)
FLAG_BENCHMARK(noatomic,relaxed,relaxed)
FLAG_BENCHMARK(noatomic,acq_rel,release)

void runbenchmarks(bool csv)
{
  output_csv = csv;

  /* Standard types */
  BENCHMARKS(load_relaxed);
  BENCHMARKS(load_acquire);
  BENCHMARKS(store_relaxed);
  BENCHMARKS(store_release);
  BENCHMARKS(exchange_relaxed);
  BENCHMARKS(exchange_acquire);
  BENCHMARKS(exchange_release);
  BENCHMARKS(exchange_acq_rel);
  BENCHMARKS(compare_exchange_weak_succ_relaxed);
  BENCHMARKS(compare_exchange_weak_succ_acquire);
  BENCHMARKS(compare_exchange_weak_succ_release);
  BENCHMARKS(compare_exchange_weak_succ_acq_rel);
  BENCHMARKS(compare_exchange_weak_fail_relaxed);
  BENCHMARKS(compare_exchange_weak_fail_acquire);
  BENCHMARKS(compare_exchange_weak_fail_release);
  BENCHMARKS(compare_exchange_weak_fail_acq_rel);
  BENCHMARKS(compare_exchange_strong_succ_relaxed);
  BENCHMARKS(compare_exchange_strong_succ_acquire);
  BENCHMARKS(compare_exchange_strong_succ_release);
  BENCHMARKS(compare_exchange_strong_succ_acq_rel);
  BENCHMARKS(compare_exchange_strong_fail_relaxed);
  BENCHMARKS(compare_exchange_strong_fail_acquire);
  BENCHMARKS(compare_exchange_strong_fail_release);
  BENCHMARKS(compare_exchange_strong_fail_acq_rel);
  BENCHMARKS(add_relaxed);
  BENCHMARKS(add_acquire);
  BENCHMARKS(add_release);
  BENCHMARKS(add_acq_rel);
  BENCHMARKS(xor_relaxed);
  BENCHMARKS(xor_acquire);
  BENCHMARKS(xor_release);
  BENCHMARKS(xor_acq_rel);

  /* Atomic flag */
  BENCHMARK(flag_relaxed);
  BENCHMARK(flag_acq_rel);
}

#ifndef NOMAIN
int main(int argc,char **argv)
{
#ifdef USE_ROATOMIC
  roatomic_init((argc > 1) && strchr(argv[1],'n'));
#endif
  runbenchmarks((argc > 1) && strchr(argv[1],'c'));
}
#endif
