; Copyright 2022 RISC OS Open Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:APCS.<APCS>
        GET     Hdr:OSMisc

        GBLS    VBar
VBar    SETS    "|"

        MACRO
        LoadStaticAddress $Addr, $Reg, $Reg2
        LDR     $Reg, =$Addr
        MEND

        ; Set things up for easier standalone testing
        GBLL    AtomicSimpleStatic
AtomicSimpleStatic SETL {FALSE} ; Runtime variant switching is necessary because everything is staticly linked
        GBLL    AtomicNo26bitCode
        GBLL    AtomicNo32bitCode
AtomicNo26bitCode SETL No26bitCode ; Use the normal No26bitCode & No32bitCode definitions, to allow 32bit-neutral builds
AtomicNo32bitCode SETL No32bitCode

        AREA    |C$$Code|, CODE, READONLY
        ; Include the stdatomic implementation from these clib sources
        GET     ../../kernel/k_atomic.s

        EXPORT  roatomic_init
roatomic_init
      [ AtomicSimpleStatic
        MOV     pc, lr
      |
        FunctionEntry
        MOV     r12, a1
        MOV     r2, #0 ; Variant flags
        ADR     r3, %FT90
10
        MOV     r0, #OSPlatformFeatures_ReadCPUFeatures
        LDRB    r1, [r3], #2
        TEQ     r1, #255
        BEQ     %FT50
        SWI     XOS_PlatformFeatures
        TEQ     r0, #1
        LDREQB  r1, [r3, #-1]
        ORREQ   r2, r2, r1
        B       %BT10
50
        ; Clear the 'U' flags if r12 is zero (i.e. we're not running in unsafe
        ; mode)
        CMP     r12, #0
        BICEQ   r2, r2, #VariantFlag_UARMa+VariantFlag_UHalfword
        LDR     a1, =variant_flags
        STRB    r2, [a1]
        Return
90
        DCB     CPUFeature_ARMa,VariantFlag_ARMa+VariantFlag_UARMa
        ; HACK: Also consider anything ARMv6+ as supporting ARMa, because when deciding which routine to call/export we check from the lowest flag up, but some ARMv6+ might not support SWP
        DCB     CPUFeature_LDREX_STREX,VariantFlag_ARMa+VariantFlag_UARMa
        DCB     CPUFeature_Halfword,VariantFlag_Halfword+VariantFlag_UHalfword
        DCB     CPUFeature_ARMK,VariantFlag_ARMK
        DCB     CPUFeature_ARMv7,VariantFlag_ARMv7
        DCB     255
        ALIGN

        AREA    |C$$Data|, DATA ; Not NOINIT; for simplicity, module builds will use the data area that's part of the module image, instead of the instance-specific copy of the data
variant_flags
        DCB     0
      ]

      [ AtomicNoARMK
        AREA    |C$$Data2|, DATA ; Not NOINIT; for simplicity, module builds will use the data area that's part of the module image, instead of the instance-specific copy of the data
        ; Anything which can't guarantee ARMK will need the spinlock
StaticData
O_atomic_spinlock * 0
        DCB     0
      ]

        END
